
/*  Programmer: Brandon Campbell
 *  Date:       1/25/2018
 *  Project:    Homework 1
 *  Purpose:    Serves as a framework for future homework. Takes 1 or 2 arguments and
 *              searches for a "Setup File". Then puts the contents of that setup file
 *              into a Log File.
 */

// Include the C++ i/o library
#include <iostream>
// Include the C-string library
#include <string.h>
// Standard library
#include <stdlib.h>
// Unix library
#include <unistd.h>
#include "SetupData.h"
#include "Log.h"
#include "Message.h"

// Shortcut for the std namespace
using namespace std;

// String length, plus one for \0
#define STRLEN 32

int openCommandFile(string cmdFile);
int readCmdFile(Message &msg);


fstream cmdStream;
int msgCounter;

//
// main( )
//
//    Shows the use of argc and argv
//      and retrieving argv from getopt( )
//
int main( int argc, char** argv )
{
    //SetupData setupData;
    Log log;
    Message msg;
    string dataString;
    string strPathToSetupFile;
    string strNameOfSetupFile;
    char* tempMsgString;
    int setupDataIsPresent;

    // getopt( ) requires char* arguments
    char pathToSetupFile[STRLEN] = "", nameOfSetupFile[STRLEN] = "";
    // For return value of getopt( )
    char argFlag;


    // Use getopt( ) to loop through -p (path to find setupfile), -s (setupfilename -- optional) flags
    while ( (argFlag = getopt(argc, argv, "p:s:")) != -1 )
    {
        switch (argFlag)
        {
            case 'p': strncpy(pathToSetupFile, optarg, STRLEN-1);
                break;
            case 's': strncpy(nameOfSetupFile, optarg, STRLEN-1);
                break;
            default:
                break;
        }; // switch
    } // while

    //instantiate these strings to properly pass in strings to the overloaded constructor.
    strPathToSetupFile = pathToSetupFile;
    strNameOfSetupFile = nameOfSetupFile;
    //if we got a path in the arguments, put it in the SetupData
    if (!strlen(pathToSetupFile) > 0)
    {
        cout << "Usage: hw1 -p <pathname> [-s <setupfilename>]" << endl;
        exit(1);
    }

    SetupData setupData(strPathToSetupFile, strNameOfSetupFile);

    //if we got a setupfile name, put it in the SetupData
    if(strlen(nameOfSetupFile) > 0)
    {
        setupData.setSetupfilename(nameOfSetupFile);
    }
    else    //otherwise put the Default setupfile name in: "setupfile"
    {
        setupData.setSetupfilename("setupfile");
    }

    setupDataIsPresent = setupData.open();  //see if we can open the setupdata file. . .
    if(setupDataIsPresent == 1) //check if we could open the file--if not, write an error message.
    {
        setupData.read();
        //store the SetupFile data into a temporary string for writing to the logfile.


        if(!setupData.getLogfilename().empty()) //if the setupdata contains some logfile name, replace the default.
        {
            log.setLogfileName(setupData.getLogfilename());
        }

        log.open();
        //Write the log records for the command file to the log file.
        log.writeLogRecord("pathname: " + setupData.getPathname() + "\n");
        log.writeLogRecord("setupfilename: " + setupData.getSetupfilename() + "\n");
        log.writeLogRecord("logfile: " + setupData.getLogfilename() + "\n");
        log.writeLogRecord("commandfile: " + setupData.getCommandfilename() + "\n");
        log.writeLogRecord("username: " + setupData.getUsername() + "\n\n");
        //Begin opening of the commandfile.

        //attempt to open the command file and load in its data...
        if (openCommandFile(setupData.getCommandfilename().c_str()) == 1)
        {
            string firstLine;
            while(cmdStream.peek() != -1)
            {
                if(readCmdFile(msg) == 1)
                {
                    log.writeLogRecord("Message ID: " + to_string(msg.id) + "\n");
                    log.writeLogRecord("Command: " + string(1, msg.command) + "\n");
                    log.writeLogRecord("Key: " + string(msg.key) + "\n");
                    log.writeLogRecord("Payload: " + string(msg.payload) + "\n");
                }

            }
            cmdStream.close();

        }
        else    //of not found/able to read, print an error msg. to console and to the logfile.
        {
            log.writeLogRecord("Could not find/read the command file: \"" + setupData.getCommandfilename());
            cout << "Could not find/read the command file: \"" + setupData.getCommandfilename() + "\"" << endl;
        }


        log.close();

        setupData.print();
        setupData.close();

        return 0;
    }
    else //if we were not able to read/find the setupdata file, write such in the logfile, tell user, and exit.
    {
        dataString = "Set up file " + setupData.getSetupfilename() + " does not exist!";
        log.open();

        log.writeLogRecord(dataString);

        log.close();
        cout << "Could not find/read the setupfile: \"" + setupData.getSetupfilename() + "\"" << endl;
        exit(1);
    }

} // main( )

/*
 * open()
 * Opens the directory that is listed in pathName, then reads the setupfile located there
 */
int openCommandFile(string cmdFilePath)
{
    int returnValue = 0;

    cmdStream.open(cmdFilePath, ios::in);
    if (!cmdStream)  //otherwise we couldn't find/open the file, return 0.
    {
        returnValue = 0;
    } else    //if the file opens, it must be present, return 1.
    {
        returnValue = 1;
    }

    return returnValue;
}

/*
* read()
* Reads the data within the setup file that is currently in the filestream
*/
int readCmdFile(Message &msg)
{
    string line;    //holds the temporary string that is received from the getline

    //first check to see if the setupdata is in the standard format: i.e. logfile: mylog, if so
    //parse with a normal cin style

    getline(cmdStream, line);

        if(!line.empty())
        {
            msg.id = ++msgCounter;
            msg.command = char(line[0]);

            getline(cmdStream, line);
            strcpy(msg.key, line.c_str());

            getline(cmdStream, line);
            strcpy(msg.payload, line.c_str());

            return 1;
        } else
            return 0;

}
/*                                          */
/* Programmer: Brandon Campbell             */
/* Date  : 1/23/2018                        */
/*                                          */
/* Purpose: Writes log records with the     */
/*          two-line format:                */
/*          <timestamp>                     */
/*          <data>                          */
/*                                          */

// Include the C++ i/o library
#include <iostream>
// Include the C-string library
#include <string.h>
// Standard library
#include <stdlib.h>
// Unix library
#include <unistd.h>
#include "Log.h"
// Shortcut for the std namespace
using namespace std;

const string Log::DEFAULT_LOG_FILE_NAME = "log.txt";

/*
 * Log()
 * DEFAULT CONSTRUCTOR
 * Sets the logfilename to the default.
 */
Log::Log()
{
    logfilename = DEFAULT_LOG_FILE_NAME;
}

/*
 * Log(char *)
 * OVERLOADED CONSTRUCTOR
 * Takes in the name of the log file (in the form of a char * location (string),
 * creates the LogFile, and sets the name to the passed in argument
 */
Log::Log(char *lname)
{
    logfilename = lname;
}

/*
 * Log(string)
 * OVERLOADED CONSTRUCTOR
 * Takes in the name of the log file as a string, creates the LogFile and sets the name
 * to the passed in argument
 */
Log::Log(string lname)
{
    logfilename = lname;
}

/*
 * close()
 * Attempts to close the opened logF filestream.
 * Returns 0 for FAILURE and 1 for SUCCESS.
 */
int Log::close()
{

    logF << "\n" << getTimeStamp() << "End\n\n" << endl;
    //logF << "End" << endl;
    logF.close();
    if(!logF)
    {
        return 1;
    }
    else
    {
        return 0;
    }

}

/*
 * getDefaultLogfileName()
 * Simply returns the constant string: DEFAULT_LOG_FILE_NAME
 */
string Log::getDefaultLogfileName()
{
    return DEFAULT_LOG_FILE_NAME;
}


/*
 * getLogfileName()
 * Simply returns the name of the logfile.
 */
string Log::getLogfileName()
{
    return logfilename;
}

/*
 * getTimeStamp()
 * Simply gets and returns the current time in a formatted
 * style.
 */
string Log::getTimeStamp()
{
    time_t timeStamp;

    time(&timeStamp); //get the actual timestamp

    return ctime(&timeStamp);   //format and return the timestamp
}

/*
 * open()
 * Attempts to open the logfile by its logfilename by loading in the filestream.
 * Returns a 0 for FAILURE and a 1 for SUCCESS.
 */
int Log::open()
{
    logF.open(logfilename.c_str(), ios::app);

    //Write the beginning of the logfile.
    if(!logF)
    {
        return 1;
    }
    else
    {
        logF << getTimeStamp() << "Begin:\n" << endl;
        return 0;
    }
}

/*
 * setLogfileName(string)
 * Simply takes in a string and sets it to be the log file's name.
 */
void Log::setLogfileName(string cname)
{
    logfilename = cname;
}

/*
 * writeLogRecord(string)
 * Writes whatever string is passed into it into the log file that is currently open.
 * Returns 0 for FAILURE and 1 for SUCCESS
 */
int Log::writeLogRecord(string s)
{
    logF << getTimeStamp() << s << endl;
}

